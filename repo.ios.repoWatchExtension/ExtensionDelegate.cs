﻿//
//  ExtensionDelegate.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;

using Foundation;
using WatchKit;

namespace repo.ios.repoWatchExtension {
  [Register( "ExtensionDelegate" )]
  public class ExtensionDelegate : WKExtensionDelegate {
    public override void ApplicationDidFinishLaunching ( ) {
      // Perform any final initialization of your application.
    }

    public override void ApplicationDidBecomeActive ( ) {
      // Restart any tasks that were paused (or not yet started) while the application was inactive.
      // If the application was previously in the background, optionally refresh the user interface.
    }

    public override void ApplicationWillResignActive ( ) {
      // Sent when the application is about to move from active to inactive state.
      // This can occur for certain types of temporary interruptions
      // (such as an incoming phone call or SMS message) or when the user quits the application
      // and it begins the transition to the background state.
      // Use this method to pause ongoing tasks, disable timers, etc.
    }
  }
}

