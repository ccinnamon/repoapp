﻿//
//  MainForm.eto.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using Eto.Forms;
using Eto.Drawing;

namespace repo.desktop {
  /// <summary>
  /// Your application's main form
  /// </summary>
  public partial class MainForm : Form {
    void InitializeComponent ( ) {
      Title = "My Eto Form";
      ClientSize = new Size( 400, 350 );

      Content = new StackLayout {
        Items =
        {
          "Hello World!",
          // Add more controls here
        }
      };

      // create a few commands that can be used for the menu and toolbar
      var clickMe = new Command { MenuText = "Click Me!", ToolBarText = "Click Me!" };
      clickMe.Executed += ( sender, e ) => MessageBox.Show( this, "I was clicked!" );

      var quitCommand = new Command { MenuText = "Quit", Shortcut = Application.Instance.CommonModifier | Keys.Q };
      quitCommand.Executed += ( sender, e ) => Application.Instance.Quit( );

      var aboutCommand = new Command { MenuText = "About..." };
      aboutCommand.Executed += ( sender, e ) => MessageBox.Show( this, "About my app..." );

      // create menu
      Menu = new MenuBar {
        Items = {
          // File submenu
          new ButtonMenuItem { Text = "&File", Items = { clickMe } },
          // new ButtonMenuItem { Text = "&Edit", Items = { /* commands/items */ } },
          // new ButtonMenuItem { Text = "&View", Items = { /* commands/items */ } },
        },
        ApplicationItems = {
          // application (OS X) or file menu (others)
          new ButtonMenuItem { Text = "&Preferences..." },
        },
        QuitItem = quitCommand,
        AboutItem = aboutCommand
      };

      // create toolbar      
      ToolBar = new ToolBar { Items = { clickMe } };
    }
  }
}
